from numpy.distutils.core import Extension

F90FLAGS = "-fopenmp -O3 -march=native -Wa,-q".split()
_chemise = Extension(name = '_chemise',
                     libraries = ['gomp',],
                     extra_f90_compile_args=F90FLAGS,
                     sources = ['_chemise.f90'])

if __name__ == "__main__":
    from numpy.distutils.core import setup

    setup(name = 'chemise',
          description       = "Chemical reactions in Python",
          author            = "Alejandro Luque",
          author_email      = "aluque@iaa.es",
          data_files=[('templates', ['templates/chemise.cu.tmpl'])],
          py_modules  = ['chemise'],
          ext_modules = [_chemise]
          )
