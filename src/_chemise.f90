! Specialized function to interpolate in 1d arrays
subroutine uinterpol1d(x0, dx, y, newx, newy, l, k)
  implicit none 
  integer l
  integer i, k
  
  real*8  x0, dx, y(k), newx(l), newy(l)
  real*8  v, f
  integer i0, i1
  !f2py intent(in) newx(l)
  !f2py intent(out) newy(l)
  !f2py integer intent(hide), depend(newx) :: l=shape(newx, 0)
  !f2py integer intent(hide), depend(y) :: k=shape(y, 0)
  !$OMP PARALLEL DO PRIVATE(v, i0, i1, f)
  do i=1, l
     v = (newx(i) - x0) / dx
     i0 = floor(v)
     i1 = ceiling(v)
          
     if (i0 .le. 0) then
        newy(i) = y(1)
        cycle
     endif
     
     if (i1 + 1 .gt. k) then
        newy(i) = y(k)
        cycle
     endif
     
     f = v - i0
     
     newy(i) = y(i0 + 1) * (1. - f) + y(i1 + 1) * f
  end do
  !$OMP END PARALLEL DO
end subroutine uinterpol1d


! Calculate derivatives
subroutine derivs(n, k, F, Flen, RV, RC, RP, nreactions, nspecies, ndepth, d)
  implicit none 

  integer, intent(in) :: nreactions, nspecies, ndepth
  
  real(kind=8), dimension(ndepth, nspecies), intent(in) :: n
  integer, dimension(nreactions, 16), intent(in) :: F
  integer, dimension(nreactions), intent(in) :: Flen
  
  real(kind=8), dimension(ndepth, nreactions), intent(in) :: k

  ! The matrix R is passed in csr format: RV contains values,
  ! RC the columns and RP pointers to the first element of each row.
  real(kind=8), dimension(:), intent(in) :: RV
  integer, dimension(:), intent(in) :: RC, RP

  real(kind=8), dimension(ndepth, nspecies), intent(out) :: d

  real(kind=8), dimension(ndepth, nreactions) :: nprod
  integer i, j

  do i = 1, nreactions
     if (Flen(i) == 0) then
        ! Here we allow "spontaneous production" of some species which,
        ! although strange may be useful sometimes.
        nprod(:, i) = 1.0
     end if
     
     do j = 1, Flen(i)
        if (j == 1) then
           nprod(:, i) = n(:, F(i, j) + 1)
        else
           nprod(:, i) = nprod(:, i) * n(:, F(i, j) + 1)
        end if
     end do
  end do

  
  do i = 1, nspecies
     d(:, i) = 0.0
     
     do j = RP(i) + 1, RP(i + 1)        
        d(:, i) = d(:, i) + RV(j) * nprod(:, RC(j) + 1) * k(:, RC(j) + 1)
     end do
  end do
  
end subroutine derivs
        

subroutine jacobian(n, rates, F, Flen, RV, RC, RP, &
     &MV, MC, MP, nreactions, nspecies, ndepth, jac)

  implicit none 
  integer, intent(in) :: nreactions, nspecies, ndepth
  
  real(kind=8), dimension(ndepth, nspecies), intent(in) :: n
  integer, dimension(nreactions, 16), intent(in) :: F
  integer, dimension(nreactions), intent(in) :: Flen
  
  real(kind=8), dimension(ndepth, nreactions), intent(in) :: rates

  ! The matrices R and M are passed in csr format: RV contains values,
  ! RC the columns and RP pointers to the first element of each row.
  ! Note that we are multiplying R * M.T so M does not need to be converted into
  ! csc.
  real(kind=8), dimension(:), intent(in) :: RV
  integer, dimension(:), intent(in) :: RC, RP, MV, MC, MP

  real(kind=8), dimension(ndepth, nspecies, nspecies), intent(out) :: jac

  real(kind=8), dimension(ndepth, size(MV)) :: nprod
  integer i, j, k, l, inspec
  logical skipped
  
  do i = 1, size(MV)
     nprod(:, i) = MV(i)
  end do
  
  do i = 1, nspecies
     do j = MP(i) + 1, MP(i + 1)
        skipped = .false.

        ! We iterate now over all species that affect reaction MC(j) + 1
        do k = 1, Flen(MC(j) + 1)
           ! This is each of thes species
           inspec = F(MC(j) + 1, k) + 1

           ! We remove one item of the species i b.c. we are differentiating
           ! over this density.
           if (.not. skipped .and. (i == inspec)) then
              skipped = .true.
           else
              nprod(:, j) = nprod(:, j) * n(:, inspec)
           end if
        end do
     end do
  end do

  do i = 1, nspecies
     do j = 1, nspecies
        jac(:, i, j) = 0
     
        ! Now find the coincidences between the columns in R and M.
        k = RP(i) + 1
        l = MP(j) + 1
        do while ((k .le. RP(i + 1)) .and. (l .le. MP(j + 1)))
           if (RC(k) == MC(l)) then
              ! Match!
              jac(:, i, j) = jac(:, i, j) &
                   &+ RV(k) * rates(:, MC(l) + 1) * nprod(:, l)
              k = k + 1
              l = l + 1
           else if (RC(k) < MC(l)) then
              k = k + 1
           else
              l = l + 1
           end if
           
        end do
     end do
  end do
end subroutine jacobian
